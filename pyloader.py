# Script tu run the application

from pyloader import clipboard, downloader

def main():
    print("Running actors...")
    actor_clip = clipboard.ClipboardChecker.start()

    print("Press ENTER to quit...\n\n")
    trash = input()
    print("Stopping worker threads...")

    actor_clip.stop()
    print("Done.")

main()
