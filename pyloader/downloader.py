import pykka
import requests

from pathlib import Path
from urllib.parse import urlsplit

class Downloader(pykka.ThreadingActor):
    def __init__(self, path="./", default_filename='download.bin'):
        super().__init__()
        self.path = path
        self.default_filename = default_filename

    def on_receive(self, message):
        # Expecting an URL in message
        print(f"Downloader: download '{message}'")

        # Guess filename from the URL
        filename: str = Downloader.get_filename(message, default_filename=self.default_filename)
        print(f"Downloader: filename = {filename}")

        # Download the file
        req = requests.get(message, timeout=(15, 5), allow_redirects=True, stream=True)
        print(f"Downloader: HTTP request sent")
        if req.status_code == requests.codes.ok:
            # If name returned in response, use it.
            if 'Content-Disposition' in req.headers:
                name: str = req.headers['Content-Disposition'].split('filename=')[1].strip('\'"')
                if name != "":
                    filename = name
            with open(Path(self.path) / filename, "wb") as f:
                for chunk in req.iter_content(chunk_size=4096):
                    if chunk:
                        f.write(chunk)
            print(f"Downloader: {filename} saved successfully.")
        else:
            # Report error
            print(f"Downloader: error; code = {req.status}")

    @staticmethod
    def get_filename(url: str, default_filename: str) -> str:
        result = ''
        try:
            result = Path(urlsplit(url).path).name
            if result.strip() == '':
                result = default_filename
        except AttributeError:
            result = default_filename
        return result

if __name__ == "__main__":
    url = 'https://2.python-requests.org/en/master/_static/requests-sidebar.jpg'
    req = requests.get(url, allow_redirects=True)
    pass