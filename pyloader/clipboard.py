try:
    import tkinter as tk    # Python 3.x+
except ImportError:
    import Tkinter as tk    # type: ignore

import pykka
import re
import time

from typing import List

from pyloader.downloader import Downloader

def get_clipboard_text():
    # Source: https://stackoverflow.com/a/49646482/5583078
    root = tk.Tk()
    # keep the window from showing
    root.withdraw()
    return root.clipboard_get()

class ClipboardChecker(pykka.ThreadingActor):
    MSG_TICK = "tick"

    def __init__(self, period: float=0.2):
        super().__init__()
        self.period: float = period
        self._last_data: str = ''
        self.lst_downloaders: List[Downloader] = list()

    def on_start(self):
        self.actor_ref.tell(ClipboardChecker.MSG_TICK)

    def on_stop(self):
        for worker in self.lst_downloaders:
            worker.stop()

    def on_receive(self, message: str):
        if message.lower() == ClipboardChecker.MSG_TICK:
            data = get_clipboard_text()
            if self._last_data != data and re.match("^http(s)://", data, re.IGNORECASE):
                worker = Downloader.start(path="./download")
                self.lst_downloaders.append(worker)
                worker.tell(data)
                self._last_data = data
            else:
                # Data has been already processed
                pass
        time.sleep(self.period)
        self.actor_ref.tell(ClipboardChecker.MSG_TICK)
